import React from "react";
import { View, Text, TouchableOpacity, Easing, Animated } from "react-native";
import {
  createStackNavigator,
  createAppContainer,
  NavigationEvents,
  createDrawerNavigator,
} from "react-navigation";
import Login from "./components/uikit/Login";
import Section from "./components/uikit/section";
import YouTube from "./components/uikit/youtube";
import Articles from "./components/uikit/articles";
import PresentationList from "./components/screens/PresentationList";
import Result from "./components/uikit/Result";
import Test from "./components/screens/Test";
import Exam from "./components/screens/Exam";
import TextViewer from "./components/screens/TextViewer";
import PresentationViewer from "./components/screens/PresentationViewer";
import Letter from "./components/screens/Letter";
import TestResult from "./components/screens/TestResult";
import SideMenu from "./components/navigation/SideMenu";
import SideMenuForText from "./components/navigation/SideMenuForText";
import SideMenuForPresentation from "./components/navigation/SideMenuForPresentation";
import Courses from "./components/screens/Courses";
import SingleCourse from "./components/screens/SingleCourse";
import TextList from "./components/screens/TextList";
import PresentationsList from "./components/uikit/presentationsList";
import Slideshow from "./components/uikit/Slideshow";
import Others from "./components/uikit/Others";

const transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 750,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps;

      const thisSceneIndex = scene.index;
      const width = layout.initWidth;

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [width, 0],
      });

      return { transform: [{ translateX }] };
    },
  };
};
const transitionConfigDrawer = () => {
  return {
    transitionSpec: {
      duration: 750,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps;

      const thisSceneIndex = scene.index;
      const width = layout.initWidth;

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [width, 0],
      });

      return { transform: [{ translateX }] };
    },
  };
};
const DrawerNav = createDrawerNavigator(
  {
    Section: Section,
    Letter: Letter,
  },
  {
    initialRouteName: "Section",
    contentComponent: SideMenu,
    transitionConfigDrawer,
  }
);
const DrawerTextViewer = createDrawerNavigator(
  {
    TextViewer: TextViewer,
  },
  {
    initialRouteName: "TextViewer",
    contentComponent: SideMenuForText,
    transitionConfigDrawer,
  }
);
const DrawerPresentationViewer = createDrawerNavigator(
  {
    Slideshow: Slideshow,
  },
  {
    initialRouteName: "Slideshow",
    contentComponent: SideMenuForPresentation,
    transitionConfigDrawer,
  }
);
const AppNavigator = createStackNavigator(
  {
    Login: Login,
    Drawer: DrawerNav,
    SingleCourse: SingleCourse,
    PresentationsList: PresentationsList,
    Articles: Articles,
    Others: Others,
    YouTube: YouTube,
    Test: Test,
    Exam: Exam,
    TestResult: TestResult,
    DrawerText: DrawerTextViewer,
    DrawerSlideshow: DrawerPresentationViewer,
    PresentationViewer: PresentationViewer,
  },
  {
    initialRouteName: "Login",
    transitionConfig,
  }
);
AppNavigator.navigationOptions = { header: null };
DrawerNav.navigationOptions = { header: null };
DrawerTextViewer.navigationOptions = { header: null };
DrawerPresentationViewer.navigationOptions = { header: null };

export default createAppContainer(AppNavigator);
