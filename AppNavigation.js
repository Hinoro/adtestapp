import React from "react";
import { View, Text, TouchableOpacity, Easing, Animated } from "react-native";
import {
  createStackNavigator,
  createAppContainer,
  NavigationEvents,
  createDrawerNavigator
} from "react-navigation";
import Login from "./components/screens/Login";
import Courses from "./components/screens/Courses";
import SingleCourse from "./components/screens/SingleCourse";
import PresentationList from "./components/screens/PresentationList";
import TextList from "./components/screens/TextList";
import VideoList from "./components/screens/VideoList";
import Test from "./components/screens/Test";
import TextViewer from "./components/screens/TextViewer";
import PresentationViewer from "./components/screens/PresentationViewer";
import Letter from "./components/screens/Letter";
import TestResult from "./components/screens/TestResult";
import SideMenu from "./components/navigation/SideMenu";
import SideMenuForText from "./components/navigation/SideMenuForText";
const transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 750,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps;

      const thisSceneIndex = scene.index;
      const width = layout.initWidth;

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [width, 0]
      });

      return { transform: [{ translateX }] };
    }
  };
};
const transitionConfigDrawer = () => {
  return {
    transitionSpec: {
      duration: 750,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps;

      const thisSceneIndex = scene.index;
      const width = layout.initWidth;

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [width, 0]
      });

      return { transform: [{ translateX }] };
    }
  };
};
const DrawerNav = createDrawerNavigator(
  {
    Courses: Courses,
    Letter: Letter
  },
  {
    initialRouteName: "Courses",
    contentComponent: SideMenu,
    transitionConfigDrawer
  }
);
const DrawerTextViewer = createDrawerNavigator(
  {
    TextViewer: TextViewer
  },
  {
    initialRouteName: "TextViewer",
    contentComponent: SideMenuForText,
    transitionConfigDrawer
  }
);
const AppNavigator = createStackNavigator(
  {
    Login: Login,
    Drawer: DrawerNav,
    SingleCourse: SingleCourse,
    PresentationList: PresentationList,
    TextList: TextList,
    VideoList: VideoList,
    Test: Test,
    TestResult: TestResult,
    DrawerText: DrawerTextViewer,
    PresentationViewer: PresentationViewer
  },
  { 
    initialRouteName: "Test",
    transitionConfig
  }
);
AppNavigator.navigationOptions = { header: null };
DrawerNav.navigationOptions = { header: null };
DrawerTextViewer.navigationOptions = { header: null };

export default createAppContainer(AppNavigator);
