export const darkblue = "#033b8a"
export const red = "#f44336"
export const green = "#43a047"
export const skyblue = "#dcebff"
export const lightblue = "#3986f0"
export const white = "#fcfcfc"
export const opacityblack = "rgba(23, 23, 23,0.72)"
export const metalgrey = "#78909c"
export const grey = "#bababa"
export const textblack = "#252a2b"
export const orange = "#e6782e"



