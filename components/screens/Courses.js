import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
export default class Courses extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
          <Text>Open drawer</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("SingleCourse")}
        >
          <Text>Courses</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
