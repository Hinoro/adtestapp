import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
export default class SingleCourse extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text style={{ fontSize: 22 }}>SingleCourse</Text>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("VideoList")}
        >
          <Text>VideoList</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("TextList")}
        >
          <Text>TextList</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("PresentationList")}
        >
          <Text>PresentationList</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("TextList")}
        >
          <Text>AdditinalMaterial</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("Test")}
        >
          <Text>Test</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("Test")}
        >
          <Text>Exam</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
