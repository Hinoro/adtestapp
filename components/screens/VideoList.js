import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
export default class VideoList extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("SingleCourse")}
        >
          <Text>VideoList</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
