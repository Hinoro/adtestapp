import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
export default class Letter extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("Courses")}
        >
          <Text>Letter</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
