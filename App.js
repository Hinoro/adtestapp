import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Dimensions } from "react-native";
import AppNavigation from "./AppNavigator";
import AppNavigator from "./AppNavigator";
// import { YellowBox } from "react-native";
import { win } from "./components/constants";
import Orientation from "react-native-orientation";
import DeviceInfo from "react-native-device-info";
// import console = require("console");

export default class App extends Component {
  constructor(props) {
    super(props);
    // console.ignoredYellowBox = ["Warning: Each", "Warning: Failed", "Possible"];
  }
  componentDidMount() {
    if (DeviceInfo.isTablet() == false) {
      console.log(DeviceInfo.isTablet());
      Orientation.lockToPortrait();
    } else {
      Orientation.unlockAllOrientations();
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <AppNavigator />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    // alignItems: 'center',
    backgroundColor: "#fcfcfc",
  },
});
